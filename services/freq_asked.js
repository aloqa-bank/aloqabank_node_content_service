const FreqAskedStorage = require("../storage/mongo/freq_asked");
const CatchWrapService = require("../wrappers/service");
const namespace = "Service.FreqAsked";


const FreqAskedService = {
    Create: CatchWrapService(`${namespace}.Create`, FreqAskedStorage.Create),
    GetByID: CatchWrapService(`${namespace}.GetByID`, FreqAskedStorage.GetByID),
    GetList: CatchWrapService(`${namespace}.GetList`, FreqAskedStorage.GetList),
    Update: CatchWrapService(`${namespace}.Update`, FreqAskedStorage.Update),
    Delete: CatchWrapService(`${namespace}.Delete`, FreqAskedStorage.Delete),
};

module.exports = FreqAskedService;