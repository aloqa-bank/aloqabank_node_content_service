const FreqAskedModel = require("../../models/freq_asked");
const CatchWrapDb = require("../../wrappers/db");

const namespace = "Storage.FreqAsked";
const FreqAskedStorage = {
    Create: CatchWrapDb(`${namespace}.Create`, async (args) => {
        let countFreqAsked = await FreqAskedModel.countDocuments();

        let freqAsked = new FreqAskedModel(args);
        freqAsked.order_number = countFreqAsked + 1;

        const resp = await freqAsked.save();
        return resp;
    }),

    GetByID: CatchWrapDb(`${namespace}.GetByID`, async (args) => {
        if (!args.id) { throw new Error("id is required to get freqAsked") };

        let freqAsked = await FreqAskedModel.findOne({ _id: args.id });

        if (!freqAsked) { throw new Error("failed to find freqAsked with given id") };

        return freqAsked;
    }),

   GetList: CatchWrapDb(`${namespace}.GetList`, async (args) => {
        let query = {};

        if (args.search.trim()) {
            query = {
                ...query,
                title: { $regex: ".*" + args.search + ".*" },  
            };
        };

        let options = {
            limit: args.limit,
            skip: args.offset
        };

        return {
            freq_ask: await FreqAskedModel.find(query, {}, options),
            count: await FreqAskedModel.countDocuments(query)
        };
    }),

    Update: CatchWrapDb(`${namespace}.Update`, async (args) => {
        if (!args.id) { throw new Error("id is required to update freqAsked") }

        let freqAsked = await FreqAskedModel.findOneAndUpdate(
            {   //query
                _id: args.id,
            },
            {   //update
                $set: {
                    title: args.title,
                    description: args.description,

                }
            },
            {   //options
                upsert: false,
                new: true,
            }
        );

        if (!freqAsked) { throw new Error(`freqAsked with given id is not found!`) };

        return freqAsked;
    }),

    Delete: CatchWrapDb(`${namespace}.Delete`, async (args) => {
        if (!args.id) { throw new Error("id is required to delete freqAsked") }
        let freqAsked = await FreqAskedModel.findOneAndDelete({ _id: args.id });
        if (!freqAsked) { throw new Error(`freqAsked with given id is not found!`) };

        return {};
    }),
}
module.exports = FreqAskedStorage;