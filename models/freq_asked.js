const mongoose = require("mongoose")
const { v4 } = require("uuid")
const FreqAskedSchema = new mongoose.Schema(
    {
        _id: {
            type: String,
            default: v4,
        },
        title: {
            type: String,
        },
        description: {
            type: String,
            minlength:3
        }
    },
    {
        timestamps: { createdAt: "created_at", updatedAt: "updated_at" },
        toObject: {
            virtuals: true
        },
        toJSON: {
            virtuals: true
        }
    }
);

module.exports = mongoose.model("Freq_Asked", FreqAskedSchema);
